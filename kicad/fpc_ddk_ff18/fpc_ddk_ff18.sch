EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x08_Female J2
U 1 1 5EB6ADE1
P 6200 4900
F 0 "J2" H 6050 5350 50  0000 L CNN
F 1 "DDK_FF18-10A-R11A-3H_1x8_P0.4mm_Horizontal" H 6050 4400 50  0000 L CNN
F 2 "DDK_FF18-10A-R11A-3H_1x8_P0.4mm_Horizontal:DDK_FF18-10A-R11A-3H_1x8_P0.4mm_Horizontal" H 6200 4900 50  0001 C CNN
F 3 "~" H 6200 4900 50  0001 C CNN
	1    6200 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Male J1
U 1 1 5EB6B92C
P 5350 4900
F 0 "J1" H 5400 5350 50  0000 C CNN
F 1 "Conn_01x08_Male" H 5350 4400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 5350 4900 50  0001 C CNN
F 3 "~" H 5350 4900 50  0001 C CNN
	1    5350 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4600 5550 4600
Wire Wire Line
	6000 4700 5550 4700
Wire Wire Line
	6000 4800 5550 4800
Wire Wire Line
	6000 4900 5550 4900
Wire Wire Line
	6000 5000 5550 5000
Wire Wire Line
	6000 5100 5550 5100
Wire Wire Line
	6000 5200 5550 5200
Wire Wire Line
	6000 5300 5550 5300
$EndSCHEMATC
