/* 
 * Source code copied and modified from the Linux kernel.
 * See licence below.
 */

/* linux/drivers/input/joystick/synaptics_touchpad.c
 *
 * Copyright (C) 2009 Sony Ericsson Mobile Communications, INC
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * <Driver name>
 *   Synaptics touchpad driver
 */
#include "xperia_play_touchpad.h"

#include <stdio.h>

#define XPT_SYN_ADDRESS(xpt, func, type, addr) \
    ((xpt)->pdt[func].base[type] + (addr))

#define XPT_REG_X_LSB   (0)
#define XPT_REG_X_MSB   (1)
#define XPT_REG_Y_LSB   (2)
#define XPT_REG_Y_MSB   (3)

#define XPT_REG_FINGER_X_MSB    (0)
#define XPT_REG_FINGER_Y_MSB    (1)
#define XPT_REG_FINGER_XY_LSB   (2)
#define XPT_REG_FINGER_XY_W     (3)
#define XPT_REG_FINGER_Z        (4)

#define XPT_SYNAPTICS_MAX_N_FINGERS 10
#define XPT_SYNAPTICS_FINGER_OFF(n,x) ((((n) / 4) + !!(n % 4)) + 5 * (x))
#define XPT_SYNAPTICS_REG_MAX XPT_SYNAPTICS_FINGER_OFF(XPT_SYNAPTICS_MAX_N_FINGERS, XPT_SYNAPTICS_MAX_N_FINGERS)


void xpt_init(xpt_data_t *xpt, uint8_t address,
              xpt_read_i2c_block_data_t read,
              xpt_write_i2c_block_data_t write,
              void *ctx)
{
    uint8_t finger;
    if ( !xpt )
    {
        return;
    }
    for ( uint16_t i = 0; i < sizeof(*xpt); i++ )
    {
        ((uint8_t*)xpt)[i] = 0x00;
    }
    xpt->i2c_address = address;
    xpt->i2c_ctx = ctx;
    xpt->read_i2c_block_data = read;
    xpt->write_i2c_block_data = write;
    xpt->pdt_init = false;
    xpt->extents_init = false;
    for ( finger = 0; finger < XPT_FINGER_COUNT; finger++)
    {
        xpt->finger[finger].pressed = false;
    }
}

void xpt_fini(xpt_data_t *xpt)
{
    // same as init
    xpt_init(xpt, 0, NULL, NULL, NULL);
}

bool xpt_reg_read(xpt_data_t *xpt, uint8_t reg, char *data, uint16_t size)
{
    if ( !xpt || !xpt->read_i2c_block_data || !data )
    {
        return false;
    }
    return xpt->read_i2c_block_data(xpt->i2c_ctx, xpt->i2c_address, reg, data, size);
}

bool xpt_reg_write(xpt_data_t *xpt, uint8_t reg, uint8_t val)
{
    return xpt_reg_write_data(xpt, reg, (const char *)&val, 1);
}

bool xpt_reg_write_data(xpt_data_t *xpt, uint8_t reg, const char *data, uint16_t size)
{
    if ( !xpt || !xpt->write_i2c_block_data || !data )
    {
        return false;
    }
    return xpt->write_i2c_block_data(xpt->i2c_ctx, xpt->i2c_address, reg, data, size);
}

bool xpt_synaptics_write(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, uint8_t val)
{
    if ( !xpt )
    {
        return false;
    }
    if ( !xpt->pdt_init )
    {
        if ( !xpt_synaptics_touchpad_read_pdt(xpt) )
        {
            return false;
        }
    }
    return xpt_reg_write(xpt, XPT_SYN_ADDRESS(xpt, func, type, addr), val);
}

bool xpt_synaptics_write_data(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, const char *buf, uint16_t len)
{
    if ( !xpt )
    {
        return false;
    }
    if ( !xpt->pdt_init )
    {
        if ( !xpt_synaptics_touchpad_read_pdt(xpt) )
        {
            return false;
        }
    }
    return xpt_reg_write_data(xpt, XPT_SYN_ADDRESS(xpt, func, type, addr), buf, len);
}

bool xpt_synaptics_read(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, char *buf, uint16_t len)
{
    if ( !xpt->pdt_init )
    {
        if ( !xpt_synaptics_touchpad_read_pdt(xpt) )
        {
            return false;
        }
    }
    return xpt_reg_read(xpt, XPT_SYN_ADDRESS(xpt, func, type, addr), buf, len);
}

bool xpt_synaptics_touchpad_read_pdt(xpt_data_t *xpt)
{
    uint8_t addr = XPT_SYNAPTICS_PDT_START - 1;
    uint8_t i;
    bool rc;

    if ( !xpt )
    {
        return false;
    }

    xpt->pdt_init = false;

    for (i = 0; i < XPT_SYNAPTICS_N_FUNCTIONS; ++i)
    {
        struct xpt_synaptics_function_descriptor fdes;
        rc = xpt_reg_read(xpt, addr--, &fdes.number, 1);
        if (!rc)
            return rc;
        rc = xpt_reg_read(xpt, addr--, &fdes.int_count, 1);
        if (!rc)
            return rc;
        rc = xpt_reg_read(xpt, addr--, &fdes.base[XPT_SYN_TYPE_DATA], 1);
        if (!rc)
            return rc;
        rc = xpt_reg_read(xpt, addr--, &fdes.base[XPT_SYN_TYPE_CTRL], 1);
        if (!rc)
            return rc;
        rc = xpt_reg_read(xpt, addr--, &fdes.base[XPT_SYN_TYPE_COMMAND], 1);
        if (!rc)
            return rc;
        rc = xpt_reg_read(xpt, addr--, &fdes.base[XPT_SYN_TYPE_QUERY], 1);
        if (!rc)
            return rc;
        switch (fdes.number) {
            case 0x01:
                memcpy(&xpt->pdt[XPT_SYN_F01], &fdes,
                    sizeof(struct xpt_synaptics_function_descriptor));
                break;
            case 0x11:
                memcpy(&xpt->pdt[XPT_SYN_F11], &fdes,
                    sizeof(struct xpt_synaptics_function_descriptor));
                break;
            case 0x34:
                memcpy(&xpt->pdt[XPT_SYN_F34], &fdes,
                    sizeof(struct xpt_synaptics_function_descriptor));
                break;
            case 0x00: // Early end of page descriptor table
                return rc;
            default:
                break;
        }
    }
    xpt->pdt_init = rc;
    return rc;
}

bool xpt_synaptics_touchpad_read_model(xpt_data_t *xpt, char *name, uint16_t size)
{
    uint8_t buf[19];
    if ( !name || !size )
    {
        return false;
    }
    name[0] = '\0';
    if ( !xpt_synaptics_read(xpt, XPT_SYNF(F01_RMI, QUERY, 0x00), buf, 17) )
    {
        return false;
    }
    uint16_t name_size = size < 6 ? size : 6;
    memcpy(name, buf + 11, name_size);
    name[name_size] = '\0';
    return true;
}

bool xpt_synaptics_touchpad_read_extents(xpt_data_t *xpt)
{
    bool rc;
    uint8_t buf[4];
    int syn_finger_count[] = {1, 2, 3, 4, 5, 10, 0, 0};

    if ( !xpt )
    {
        return false;
    }

    xpt->extents_init = false;

    rc = xpt_synaptics_read(xpt, XPT_SYNF(F11_2D, CTRL, 0x06), buf, 4);
    if (!rc)
        return rc;

    xpt->extents.x_min = 0;
    xpt->extents.y_min = 0;
    xpt->extents.x_max = (buf[XPT_REG_X_LSB] | (buf[XPT_REG_X_MSB] << 8));
    xpt->extents.y_max = (buf[XPT_REG_Y_LSB] | (buf[XPT_REG_Y_MSB] << 8));

    xpt->analog_extents.width = xpt->extents.y_max;
    xpt->analog_extents.min = ((int16_t)(0)) - ((int16_t)(xpt->analog_extents.width >> 1));
    xpt->analog_extents.max = ((int16_t)(xpt->analog_extents.width >> 1)) - 1;

    rc = xpt_synaptics_read(xpt, XPT_SYNF(F11_2D, QUERY, 0x01), buf, 1);
    if (!rc)
        return rc;

    xpt->extents.n_fingers = syn_finger_count[buf[0] & 0x7];

    xpt->extents_init = rc;
    return rc;
}

bool xpt_synaptics_touchpad_read_update(xpt_data_t *xpt)
{
    uint8_t buf[XPT_SYNAPTICS_REG_MAX];
    uint8_t i, finger, analog;
    uint16_t x, y, wy, wx;
    uint8_t *finger_buf;
    if ( !xpt )
    {
        return false;
    }
    if ( !xpt->extents_init )
    {
        if ( !xpt_synaptics_touchpad_read_extents(xpt) )
        {
            return false;
        }
    }

    if ( !xpt_synaptics_read(xpt, XPT_SYNF(F11_2D, DATA, 0x00), buf,
            XPT_SYNAPTICS_FINGER_OFF(xpt->extents.n_fingers,
            xpt->extents.n_fingers)) )
    {
        return false;
    }

    for ( finger = 0; finger < XPT_FINGER_COUNT; finger++ )
    {
        xpt->finger[finger].pressed = false;
        xpt->finger[finger].x = 0;
        xpt->finger[finger].y = 0;
    }

    finger = 0;
    for ( i = 0; i < xpt->extents.n_fingers; i++)
    {
        if (buf[i >> 2] & (0x3 << ((i % 4) << 1)))
        {
            finger_buf = buf + XPT_SYNAPTICS_FINGER_OFF(xpt->extents.n_fingers, i);
            x = (finger_buf[XPT_REG_FINGER_X_MSB] << 4) | ((finger_buf[XPT_REG_FINGER_XY_LSB] & 0x0f));
            y = (finger_buf[XPT_REG_FINGER_Y_MSB] << 4) | ((finger_buf[XPT_REG_FINGER_XY_LSB] & 0xf0) >> 4);
            wx = (finger_buf[XPT_REG_FINGER_XY_W] & 0xf);
            wy = ((finger_buf[XPT_REG_FINGER_XY_W] >> 4) & 0xf);
            if ( finger < XPT_FINGER_COUNT )
            {
                xpt->finger[finger].pressed = true;
                xpt->finger[finger].x = x;
                xpt->finger[finger].y = y;
                finger++;
            }
        }
    }

    for ( analog = 0; analog < XPT_ANALOG_COUNT; analog++ )
    {
        xpt->analog[analog].x = 0;
        xpt->analog[analog].y = 0;
    }
    for ( finger = 0; finger < XPT_FINGER_COUNT; finger++ )
    {
        if ( !xpt->finger[finger].pressed )
        {
            continue;
        }
        x = xpt->finger[finger].x;
        y = xpt->finger[finger].y;
        if ( x < xpt->analog_extents.width )
        {
            analog = XPT_ANALOG_LEFT;
        }
        else if ( x >= (xpt->extents.x_max - xpt->analog_extents.width) )
        {
            x -= (xpt->extents.x_max - xpt->analog_extents.width);
            analog = XPT_ANALOG_RIGHT;
        }
        else
        {
            continue;
        }
        xpt->analog[analog].x = ((int16_t)x) - ((int16_t)(xpt->analog_extents.width >> 1));
        xpt->analog[analog].y = ((int16_t)y) - ((int16_t)(xpt->analog_extents.width >> 1));
        if ( xpt->analog[analog].x < xpt->analog_extents.min )
        {
            xpt->analog[analog].x = xpt->analog_extents.min;
        }
        if ( xpt->analog[analog].x > xpt->analog_extents.max )
        {
            xpt->analog[analog].x = xpt->analog_extents.max;
        }
        if ( xpt->analog[analog].y < xpt->analog_extents.min )
        {
            xpt->analog[analog].y = xpt->analog_extents.min;
        }
        if ( xpt->analog[analog].y > xpt->analog_extents.max )
        {
            xpt->analog[analog].y = xpt->analog_extents.max;
        }
    }

    return true;
}
