#include <Wire.h>
#include "xperia_play_touchpad.h"

static xpt_data_t touchpad;

static bool read_i2c_block_data(void *ctx, uint8_t address, uint8_t reg, char *data, uint16_t size)
{
  memset(data, 0, size);
  Wire.beginTransmission((int)address);
  Wire.write(reg);
  byte error = Wire.endTransmission();
  if ( error )
  {
    return false;
  }
  uint16_t i = 0;
  if ( size > 0 )
  {
    Wire.requestFrom((int)address, (int)size);
    for ( i = 0; i < size; i++ )
    {
      if ( !Wire.available() )
      {
        break;
      }
      data[i] = Wire.read();
    }
  }
  return i == size;
}

static bool write_i2c_block_data(void *ctx, uint8_t address, uint8_t reg, const char *data, uint16_t size)
{
  return false;
}

void setup()
{
  Wire.begin();
  Wire.setClock(9600);
  Serial.begin(115200);
}

void loop()
{
  Serial.println("Waiting for touchpad...");
  xpt_init(&touchpad, XPT_I2C_ADDRESS, read_i2c_block_data, write_i2c_block_data, NULL);
  char model_name[10];
  if ( xpt_synaptics_touchpad_read_model(&touchpad, model_name, sizeof(model_name)) )
  {
    Serial.println(model_name);
    while ( xpt_synaptics_touchpad_read_update(&touchpad) )
    {
      printf("(%d, %d) (%d, %d)\n",
        touchpad.analog[XPT_ANALOG_LEFT].x,
        touchpad.analog[XPT_ANALOG_LEFT].y,
        touchpad.analog[XPT_ANALOG_RIGHT].x,
        touchpad.analog[XPT_ANALOG_RIGHT].y);
      delay(50);
    }
  }
  xpt_fini(&touchpad);
  delay(1000);
}
