#ifndef _H_XPERIA_PLAY_TOUCHPAD_H_
#define _H_XPERIA_PLAY_TOUCHPAD_H_

/* 
 * Source code copied and modified from the Linux kernel.
 * See licence below.
 */

/* linux/drivers/input/joystick/synaptics_touchpad.c
 *
 * Copyright (C) 2009 Sony Ericsson Mobile Communications, INC
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * <Driver name>
 *   Synaptics touchpad driver
 */

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define XPT_I2C_ADDRESS     0x20

typedef bool (*xpt_read_i2c_block_data_t)(void *, uint8_t, uint8_t, char *, uint16_t);
typedef bool (*xpt_write_i2c_block_data_t)(void *, uint8_t, uint8_t, const char *, uint16_t);

typedef uint8_t xpt_func_t;
#define XPT_SYN_F01                 ((xpt_func_t)0)
#define XPT_SYN_F11                 ((xpt_func_t)1)
#define XPT_SYN_F34                 ((xpt_func_t)2)
#define XPT_SYN_F01_RMI             (XPT_SYN_F01)
#define XPT_SYN_F11_2D              (XPT_SYN_F11)
#define XPT_SYN_F34_FLASH           (XPT_SYN_F34)
#define XPT_SYNAPTICS_N_FUNCTIONS   (3)

typedef uint8_t xpt_type_t;
#define XPT_SYN_TYPE_DATA       ((xpt_type_t)0)
#define XPT_SYN_TYPE_CTRL       ((xpt_type_t)1)
#define XPT_SYN_TYPE_COMMAND    ((xpt_type_t)2)
#define XPT_SYN_TYPE_QUERY      ((xpt_type_t)3)
#define XPT_SYNAPTICS_N_TYPES   (4)

#define XPT_SYNAPTICS_PDT_START (0xEF)

#define XPT_SYNF(x, y, a) XPT_SYN_##x, XPT_SYN_TYPE_##y, a

struct xpt_synaptics_function_descriptor {
    uint8_t number;
    uint8_t int_count;
    uint8_t base[XPT_SYNAPTICS_N_TYPES];
};

struct xpt_synaptics_extents {
    uint16_t x_min, y_min;
    uint16_t x_max, y_max;
    uint16_t n_fingers;
};

struct xpt_finger {
    bool pressed;
    uint16_t x;
    uint16_t y;
};

#define XPT_FINGER_COUNT    (2)

struct xpt_analog {
    int16_t x;
    int16_t y;
};

struct xpt_analog_extents {
    int16_t min, max;
    uint16_t width;
};

#define XPT_ANALOG_LEFT     (0)
#define XPT_ANALOG_RIGHT    (1)
#define XPT_ANALOG_COUNT    (2)

typedef struct xpt_data {
    uint8_t i2c_address;
    void *i2c_ctx;
    xpt_read_i2c_block_data_t read_i2c_block_data;
    xpt_write_i2c_block_data_t write_i2c_block_data;
    struct xpt_synaptics_function_descriptor pdt[XPT_SYNAPTICS_N_FUNCTIONS];
    bool pdt_init;
    struct xpt_synaptics_extents extents;
    struct xpt_analog_extents analog_extents;
    bool extents_init;
    struct xpt_finger finger[XPT_FINGER_COUNT];
    struct xpt_analog analog[XPT_ANALOG_COUNT];
} xpt_data_t;

void xpt_init(xpt_data_t *xpt, uint8_t address, xpt_read_i2c_block_data_t read, xpt_write_i2c_block_data_t write, void *ctx);
void xpt_fini(xpt_data_t *xpt);
bool xpt_reg_read(xpt_data_t *xpt, uint8_t reg, char *data, uint16_t size);
bool xpt_reg_write(xpt_data_t *xpt, uint8_t reg, uint8_t val);
bool xpt_reg_write_data(xpt_data_t *xpt, uint8_t reg, const char *data, uint16_t size);
bool xpt_synaptics_write(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, uint8_t val);
bool xpt_synaptics_write_data(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, const char *buf, uint16_t len);
bool xpt_synaptics_read(xpt_data_t *xpt, xpt_func_t func, xpt_type_t type, uint8_t addr, char *buf, uint16_t len);
bool xpt_synaptics_touchpad_read_pdt(xpt_data_t *xpt);
bool xpt_synaptics_touchpad_read_model(xpt_data_t *xpt, char *name, uint16_t size);
bool xpt_synaptics_touchpad_read_extents(xpt_data_t *xpt);
bool xpt_synaptics_touchpad_read_update(xpt_data_t *xpt);

#ifdef __cplusplus
};
#endif

#endif // _H_XPERIA_PLAY_TOUCHPAD_H_
