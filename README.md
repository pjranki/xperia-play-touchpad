# Xperia Play touchpad

## Manufacture
Synaptics

## Model
TM1456

## Connector
Flexible PCB Connector (FPC)
FF18-8A-R11A-3H

http://www.ddknet.co.jp/English/products/print/fpc-connectors/ff18/index.html
https://www.electroshield.com/FUJIKURA-(DDK-Electronics)-FF18-8A-R11A-3H
https://www.onlinecomponents.com/fujikura-america-inc.-ddk/ff1810ar11a3h-49360190.html
https://www.worldwayelec.com/pro/fujikura-ltd/ff18-8a-r11a-3h/4091925

## Protocol
i2c, address 0x20

## Pinout

```
CONNECTOR (from top) | PIN |            USE
_____________________|_____|____________________________
              _   _  |     |
_____________| \_/ | |     |
-------------------- |  1  | SCL (i2c)
==================== |  2  | GND (Thick track)
-------------------- |  3  | SDA (i2c)
-------------------- |  4  | VCC (I used 3.3V)
-------------------- |  5  | ?
----------.   ------ |  6  | N/C (Track not connected)
--------.  '-------- |  7  | ?
         '---------- |  8  | ?
____________________ |     |
             |_/ \_| |     |
                     |     | 
```

## Linux kernel driver
https://github.com/DooMLoRD/Xperia-2011-Kernel-2.6.32.9/tree/master/kernel
```
CONFIG_SYNAPTICS_TOUCHPAD
```
